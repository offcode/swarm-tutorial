
# Tutorial

[![Bzz and Feeds](https://asciinema.org/a/234799.svg)](https://asciinema.org/a/234799?speed=2&autoplay=1)

# Resources

## Coloring the terminal

- [gookit/color: Terminal color rendering tool library, support 8/16 colors, 256 colors, RGB color rendering output, compatible with Windows. CLI](https://github.com/gookit/color)
- [command line - Print a 256-color test pattern in the terminal - Ask Ubuntu](https://askubuntu.com/questions/821157/print-a-256-color-test-pattern-in-the-terminal)
- [terminal - Print true color (24-bit) test pattern - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/404414/print-true-color-24-bit-test-pattern/404415#404415)

## Ideas for `chex`

- robohash images
- color the background of the text
- display emojis
