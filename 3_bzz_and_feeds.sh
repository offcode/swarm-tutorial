# It takes some time until it's ready
sleep 3 
# Check if it's running
curl -s http://localhost:8500

clear ; echo "Upload a file" | lolcat
# Create a file for upload
echo "Why can't you starve in the desert?" > fun.txt
# Upload it
swarm up fun.txt
# It returns its hash. If you don't change the file, it returns the same hash.
swarm up fun.txt
# Better remember it, we'll use it later
swarm up fun.txt > OneLineHash
# Now get it back
swarm down fun.txt
# There are a bunch of schemes. Use the simplest one.
swarm down bzz:/fun.txt
# What did you expect? There could be millions of fun.txt files in the Swarm.
# Use the hash we saved earlier
swarm down bzz:/`cat OneLineHash` 
# Where is it? It's downloaded to a file that is named as the hash.
cat `cat OneLineHash`
# Download again specifying the target
swarm down bzz:/`cat OneLineHash` received.txt
# It's enough to have a browser to donwload it
curl -s http://localhost:8500/bzz:/`cat OneLineHash`/
# Anybody who has the hash can download it
curl -s http://swarm-gateways.net:8500/bzz:/`cat OneLineHash`/

clear ; echo "Change the file" | lolcat
# So you wanted to know the answer, here we go. 
# Append it to the original file
echo "Because of the sand wich is there" >> fun.txt
# Upload the file again
swarm up fun.txt > TwoLineHash
# The same file, but a different content, so the hash is different, too
diff OneLineHash TwoLineHash
# The first uploaded content is untouched
curl -s http://localhost:8500/bzz:/`cat OneLineHash`/
# And we can get the new content
curl -s http://localhost:8500/bzz:/`cat TwoLineHash`/
# The problem is we always have to send the new hash to our friends

clear ; echo "Create mutable content" | lolcat
# We'll always use the same url to get our content
curl "http://localhost:8500/bzz-feed:/?user=`cat Alice`&name=itneverchanges"
# Nothing there yet. We have to create a feed first.
swarm --bzzaccount `cat Alice` --password Password feed create --name itneverchanges
# In case you remember this hash, you can run the same command again and get the same hash
swarm --bzzaccount `cat Alice` --password Password feed create --name itneverchanges
# We'll need this hash, so save it for later use
swarm --bzzaccount `cat Alice` --password Password feed create --name itneverchanges > Neverchanges
# Check if we already have some content
curl "http://localhost:8500/bzz-feed:/?user=`cat Alice`&name=itneverchanges"
# Not yet. The feed is created, but it has no content.
# Write a simple message 
echo "They say I'm addicted to brake fluid" > Message
# We have to convert it to hex.
# Swarm feed expects a hex string. I'll tell you soon why.
cat Message | hexdump
# I had to play around with the format-string until I got the format right
cat Message | hexdump -v -e '/1 "%02x"' 
# Save it to a file
cat Message | hexdump -v -e '/1 "%02x"' > HexMessage
# Change the content of the feed we just created to this message
# Use the hash saved to Neverchanges and the content of HexMessage
swarm --bzzaccount `cat Alice` --password Password feed update --manifest `cat Neverchanges` `cat HexMessage`
# Ok, prefix the hex message with 0x
swarm --bzzaccount `cat Alice` --password Password feed update --manifest `cat Neverchanges` 0x`cat HexMessage`
# Now we can get the value of the feed
curl "http://localhost:8500/bzz-feed:/?user=`cat Alice`&name=itneverchanges"
# Change the content of the file
echo "I can stop anytime I want" >> Message
# Convert it to hex and save
cat Message | hexdump -v -e '/1 "%02x"' > HexMessage
# Update the feed
swarm --bzzaccount `cat Alice` --password Password feed update --manifest `cat Neverchanges` 0x`cat HexMessage`
# And we get the updated content
curl "http://localhost:8500/bzz-feed:/?user=`cat Alice`&name=itneverchanges"

clear ; echo "Change file content" | lolcat
# How do we upload a larger content?
# Let's create some large content out of many quotes
for i in {1..5000}; do fortune; done > BigMessage
# That's a lot
wc BigMessage
# Convert it to hex
cat BigMessage | hexdump -v -e '/1 "%02x"' > BigHexMessage
# And update the feed
swarm --bzzaccount `cat Alice` --password Password feed update --manifest `cat Neverchanges` 0x`cat BigHexMessage`
# We just hit a limitation of the shell or Go
# Upload the file to swarm and save its hash
swarm up BigMessage > UploadedBigMessage
# Update the feed to this hash
# (This is why feeds expect a hex string)
swarm --bzzaccount `cat Alice` --password Password feed update --manifest `cat Neverchanges` 0x`cat UploadedBigMessage`
# If we want to get the content now...
curl -s "http://localhost:8500/bzz-feed:/?user=`cat Alice`&name=itneverchanges"
# we would only get the hash of the uploaded file
# But if we use the "bzz" schema instead of "bzz-feed"
curl -s "http://localhost:8500/bzz:/`cat Neverchanges`/" | head
# What happened?
# Neverchanges is the hash of the feed
# We updated the feed to the hash of a file
# When we curl bzz:/<feed hash>, it gets redirected to that file
# 
# Now upload another file
echo "Same url, different content" > different.txt
swarm up different.txt > Different
# Update the feed with this different hash
swarm --bzzaccount `cat Alice` --password Password feed update --manifest `cat Neverchanges` 0x`cat Different`
# Read the same url
curl -s "http://localhost:8500/bzz:/`cat Neverchanges`/"