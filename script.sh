#!/bin/sh

cowsay "Prerequisites" | lolcat

# Install Geth 

# Install Swarm

# Create an account
echo '<secret password>' > Password
geth --verbosity 2 --password Password account new | \ 
	grep -oP '(?<={).*(?=})' | \
	tee Alice | \
	cut -c1-6

# Start Geth and Swarm
geth --verbosity 2 --syncmode light
export SWARM_ACCOUNT=`<Alice`
swarm --verbosity 2 --password Password
curl http://localhost:8500

# Upload and download
cowsay "Upload and download" | lolcat
echo '<h1>Does it work?</h1>Yes' > index.html
swarm up index.html | tee Index | cut -c1-6
swarm up index.html | tee IndexAgain | cut -c1-6
diff Index IndexAgain
cp index.html copy-of-index.html
swarm up copy-of-index.html | tee CopyOfIndex | cut -c1-6
diff Index CopyOfIndex

swarm down bzz:/`<Index` downloaded-index.html
diff index.html downloaded-index.html
swarm --bzzapi https://swarm-gateways.net down bzz:/`<Index` downloaded-remote.html

# Change content
cowsay "Change content" | lolcat
swarm --password Password feed create --name foo | tee Foo | cut -c1-6
swarm --password Password feed create --name foo | tee FooAgain | cut -c1-6
diff Foo FooAgain
echo -n 'Hello world' | tee message | hexdump -v -e '/1 "%02x"' > HexMessage
swarm --password Password feed update --name foo "0x`<HexMessage`"
curl "http://localhost:8500/bzz-feed:/?user=$SWARM_ACCOUNT&name=foo" > received-message
diff message received-message

# Use the hash of an actual file
swarm --password Password feed update --manifest `<Foo` "0x`<Index`"
dwarm down bzz:/`<Foo` new-message
#curl "http://localhost:8500/bzz:/?user=$SWARM_ACCOUNT&name=foo" > received-message

# -------------
cowsay "Change a file in the Swarm" | lolcat
# Create a file for upload
echo "Why can't you starve in the desert?" > fun.txt
# Upload it
swarm up fun.txt
# It returns its hash. If you don't change the file, it returns the same hash.
swarm up fun.txt
# Better remember it, we'll use it later
swarm up fun.txt > FunHash
# Now get it back
swarm down fun.txt
# There are a bunch of schemes. Use the simplest one.
swarm down bzz:/fun.txt
# What did you expect? There could be millions of fun.txt files in the Swarm.
# Use the hash we saved earlier
swarm down bzz:/`<FunHash` received.txt
# You bet they are the same
diff fun.txt received.txt
