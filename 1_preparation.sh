# In this screen cast, we'll upload a file to the swarm, then change it.
# I assume you're already running Geth in another console
# Start it in light mode, so it won't eat up your hard disk
# `geth --verbosity 2 --syncmode light`
# We need a few steps to get started

echo "Getting started" | lolcat
# We hate having to type the password again and again. Save it to a file.
echo '<secret password>' > Password
# Create an account using that password. Save it.
geth --verbosity 2 --password Password account new > /tmp/Alice
# The account is just a long hash
cat /tmp/Alice
# We actually need only the hash between the braces
cat /tmp/Alice | sed -e 's/^.*{//' -e 's/}//' > Alice