import json
import sys

def write_event(ts, etype, ch, output_fp):
    json.dump([ts, etype, ch], output_fp)
    output_fp.write('\n')

def is_prompt(cmd):
    return cmd.endswith('> ')

def split_user_input(input_fp, output_fp):
    output_fp.write(input_fp.readline())
    ts = 0
    was_prompt = False
    for line in input_fp:
        _, etype, cmd = json.loads(line)
        if was_prompt:
            for ch in cmd.strip():
                ts += 0.1
                write_event(ts, etype, ch, output_fp)
            write_event(ts, "o", "\r\n", output_fp)
            was_prompt = False
        elif is_prompt(cmd):
            was_prompt = True
            ts += 1
            write_event(ts, etype, cmd, output_fp)
        else:
            ts += 1
            write_event(ts, etype, cmd, output_fp)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser('Gimme a script')
    parser.add_argument('infile', default=sys.stdin, nargs='?', type=argparse.FileType('r'))
    parser.add_argument('outfile', default=sys.stdout, nargs='?', type=argparse.FileType('w'))
    args = parser.parse_args()
    
    split_user_input(args.infile, args.outfile)

